import asyncio, os, logging, time, json, uuid
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import ParseMode
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.utils.exceptions import (MessageNotModified, RestartingTelegram)
import config as cfg

logging.basicConfig(level=logging.WARNING)

bot = Bot(token=cfg.botToken)
dp = Dispatcher(bot)
dp.middleware.setup(LoggingMiddleware())

# подразумевается, что бот будет работать постоянно и непрерывно, поэтому
# нет нужды хранить сессии пользователей где бы то ни было, кроме оперативной памяти
sessions = {}

tests = {}
testsDir = 'tests'
for filename in os.listdir(testsDir): # загружаем тесты
	if '|' in filename:
		raise ValueError(f"Test's file name [{filename}] must not contain '|' character.")
	with open(os.path.join(testsDir, filename), 'r') as f:
		tests[filename] = json.load(f)

error_unknown = "Произошла ошибка, попробуйте начать новое тестирование."
error_testing_is_over = "Ошибка: Это тестирование уже завершено, попробуйте начать новое."

# приветственное сообщение и список тестов
async def show_welcome_message(chat_id, original_message=None):
	markup = InlineKeyboardMarkup()
	for filename in tests:
		markup.row(InlineKeyboardButton(tests[filename]['name'], callback_data=f'info|{filename}'))

	text = "Вас приветствует сервис психологических и социальных тестов имени Зигмунда Шломо Фрейда.\n\nК сожалению, Зигмунд в настоящий момент отсутствует, однако мы готовы предложить вам пройти несколько тестов на выбор."
	
	sessions[chat_id] = {
		"ts": time.time(),
		"test": None
	}

	if original_message == None:
		message = await bot.send_message(chat_id, text, reply_markup=markup)
	else:
		try:
			await original_message.edit_text(text, reply_markup=markup)
		except MessageNotModified:
			pass

# показать инфомрацию о тесте
async def send_test_info(original_message, filename):
	markup = InlineKeyboardMarkup()
	markup.row(InlineKeyboardButton("🧑‍💻 Перейти к тестированию", callback_data=f'begin|{filename}'))
	markup.row(InlineKeyboardButton("⬅️ Назад", callback_data=f'back|info|0'))

	description = tests[filename]['description']
	if description == None:
		description = tests[filename]['name']

	try:
		await original_message.edit_text(
			text=description,
			reply_markup=markup
		)
	except MessageNotModified:
		pass

# отправить вопрос из теста
async def send_question(original_message, question_number):
	test_id = sessions[original_message.chat.id]["test"]["uuid"]
	filename = sessions[original_message.chat.id]["test"]["filename"]

	markup = InlineKeyboardMarkup()
	row = []
	for k, v in tests[filename]['questions'][question_number]['answers'].items():
		row.append(InlineKeyboardButton(k, callback_data=f'answer|{filename}|{question_number}|{v}|{test_id}'))
	markup.row(*row)

	markup.row(InlineKeyboardButton("⬅️ Назад", callback_data=f'back|test|{question_number-1}'))

	questions = tests[filename]['questions']
	try:
		await original_message.edit_text(
			text=f"<b>Вопрос {question_number+1}/{len(questions)}:</b>\n{questions[question_number]['text']}",
			reply_markup=markup,
			parse_mode=ParseMode.HTML
		)
	except MessageNotModified:
		pass

# отправить результат теста
async def send_result(original_message):
	chat_id = original_message.chat.id
	score = sum(sessions[chat_id]["test"]["answers"])
	filename = sessions[chat_id]["test"]["filename"]
	results = tests[filename]['results']
	print(sessions[chat_id])

	result = next(iter(results))
	for k in results:
		if score <= int(k):
			result = results[k]
			break

	text = f"Тестирование завершено!\n\n{result['text']}\n\nИспользуйте /start, чтобы начать новое тестирование."

	if result['image'] == None:
		try:
			await original_message.edit_text(text=text, parse_mode=ParseMode.HTML)
		except MessageNotModified:
			pass
	else:
		await original_message.delete()
		await bot.send_photo(chat_id, photo=open(f"images/{result['image']}", 'rb'), caption=text)

# хендлер команды /start
@dp.message_handler(commands=['start'])
async def handler(message: types.Message):
	await show_welcome_message(message.chat.id, original_message=None)

# начало тестирования
@dp.callback_query_handler(lambda c: c.data.split('|')[0] == 'begin')
async def callback(query: types.CallbackQuery):
	args = query.data.split('|')

	if len(args) != 2:
		return await query.answer(error_unknown)

	filename = args[1]
	chat_id = query.message.chat.id
	sessions[chat_id] = {
		"ts": time.time(),
		"test": {
			"uuid": str(uuid.uuid4()),
			"filename": filename,
			"question_number": 0,
			"answers": [0]*len(tests[filename]['questions'])
		}
	}
	print(sessions[chat_id])
	await send_question(query.message, 0)

# информация о тесте перед его началом
@dp.callback_query_handler(lambda c: c.data.split('|')[0] == 'info')
async def callback(query: types.CallbackQuery):
	args = query.data.split('|')

	if len(args) != 2:
		return await query.answer(error_unknown)

	chat_id = query.message.chat.id
	if chat_id in sessions:
		sessions[chat_id]['ts'] = time.time()
		sessions[chat_id]['test'] = None
	else:
		return await query.answer(error_unknown)

	await send_test_info(query.message, filename=args[1])

# хендлер ответа на вопрос (при нажатии на кнопку)
@dp.callback_query_handler(lambda c: c.data.split('|')[0] == 'answer')
async def callback(query: types.CallbackQuery):
	args = query.data.split('|')

	if len(args) != 5:
		return await query.answer(error_unknown)

	filename = args[1]
	question_number = int(args[2])
	value = int(args[3])
	test_id = args[4]

	chat_id = query.message.chat.id

	if not chat_id in sessions or \
			sessions[chat_id]['test'] == None or \
			sessions[chat_id]['test']['uuid'] != test_id:
		return await query.answer(error_testing_is_over)

	sessions[chat_id]['ts'] = time.time()
	sessions[chat_id]['test']['question_number'] = question_number + 1
	sessions[chat_id]['test']['answers'][question_number] = value

	if question_number+1 < len(tests[filename]['questions']):
		await send_question(query.message, question_number + 1)
	else:
		await send_result(query.message)
		sessions[chat_id] = {
			"ts": time.time(),
			"test": None
		}

	print(sessions[chat_id])

# вернуться в меню
@dp.callback_query_handler(lambda c: c.data.split('|')[0] == 'back')
async def callback(query: types.CallbackQuery):
	args = query.data.split('|')

	if len(args) != 3:
		return await query.answer(error_unknown)

	state = args[1]
	question_number = int(args[2])
	chat_id = query.message.chat.id

	if not query.message.chat.id in sessions:
		return await query.answer(error_unknown)

	if sessions[chat_id]['test'] == None:
		await show_welcome_message(chat_id, original_message=query.message)
	else:
		if state == 'test':
			if question_number == -1:
				filename = sessions[chat_id]["test"]["filename"]
				await send_test_info(query.message, filename)
			else:
				await send_question(query.message, question_number)
		else:
			await show_welcome_message(chat_id, original_message=query.message)

	print(sessions[chat_id])

# Telegram периодически перезагружает свои сервера, однако это не должно влиять на работу бота
@dp.errors_handler(exception=RestartingTelegram)
async def restarting_telegram(update: types.Update, exception: Exception):
	pass

# удаление старых сессий (на случай утечки памяти)
async def garbage_collector():
	while True:
		await asyncio.sleep(60*60*24) # проверяем каждые сутки

		for chat_id, value in list(sessions.items()):
			if (time.time() - value["ts"]) > cfg.sessionLifetime:
				del sessions[chat_id]

def repeat(coro, loop):
	asyncio.ensure_future(coro(), loop=loop)

if __name__ == '__main__':
	loop = asyncio.get_event_loop()
	loop.call_later(0, repeat, garbage_collector, loop)
	executor.start_polling(dp, loop=loop, skip_updates=True)
